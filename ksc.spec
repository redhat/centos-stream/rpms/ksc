%global forgeurl https://github.com/RedHatOfficial/ksc
%global commitdate 20230109
%global commit 869a25c7de8ed880a72f66ae4f3e8407f1aa4114
%global shortcommit %(c=%{commit}; echo ${c:0:7})

%{?python_enable_dependency_generator}
%forgemeta -i

Name:		ksc
Version:	1.12
Release:	7%{?dist}
Summary:	Kernel source code checker
Group:		Development/Tools
AutoReqProv:	no
License:	GPL-2.0-or-later
URL:		https://github.com/RedHatOfficial/ksc
BuildArch:	noarch
Requires:	kmod
Requires:	binutils
Requires:	kernel-devel
Requires:	python3-requests
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
Source0:	https://github.com/RedHatOfficial/ksc/archive/%{commit}/%{name}-%{shortcommit}.tar.gz
Patch0:		0001-manpage.patch
Patch1:		0002-c9s-notifications.patch
Patch2:		0003-cs-fix-invalid-escape.patch

%description
A kernel module source code checker to find usage of select symbols

%prep
%forgesetup
# Fix build with setuptools 62.1
# https://github.com/RedHatOfficial/ksc/issues/3
sed -i "15i packages=[]," setup.py
%patch0 -p1
%patch1 -p1
%patch2 -p1

%build
%py3_build

%install
%py3_install
install -D ksc.1 %{buildroot}%{_mandir}/man1/ksc.1

%files
%license COPYING
%doc README PKG-INFO
%{_bindir}/ksc
%{_datadir}/ksc
%{_mandir}/man1/ksc.*
%config(noreplace) %{_sysconfdir}/ksc.conf
%{python3_sitelib}/ksc-%{version}*.egg-info

%changelog
* Mon Nov 18 2024 Čestmír Kalina <ckalina@redhat.com> - 1.12-7
- Resolves: RHEL-65759

* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - Packaging variables read or set by %forgemeta
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - Packaging variables read or set by %forgemeta
- Bump release for June 2024 mass rebuild

* Tue May 28 2024 Čestmír Kalina <ckalina@redhat.com> - 1.12-4
- Resolves: JIRA: https://issues.redhat.com/browse/RHEL-35791
- Update to the latest ksc

* Thu Jan 25 2024 Fedora Release Engineering <releng@fedoraproject.org> - Packaging variables read or set by %forgemeta
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Sun Jan 21 2024 Fedora Release Engineering <releng@fedoraproject.org> - Packaging variables read or set by %forgemeta
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Mon Jun 19 2023 Čestmír Kalina <ckalina@redhat.com> - 1.7-8
- Migrate license to SPDX

* Thu Jan 19 2023 Fedora Release Engineering <releng@fedoraproject.org> - Packaging variables read or set by %forgemeta
- Rebuilt for https://fedoraproject.org/wiki/Fedora_38_Mass_Rebuild

* Thu Jul 21 2022 Fedora Release Engineering <releng@fedoraproject.org> - Packaging variables read or set by %forgemeta
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Tue Jun 21 2022 Lumír Balhar <lbalhar@redhat.com> - 1.7-5
- Fix compatibility with newer setuptools

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - Packaging variables read or set by %forgemeta
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - Packaging variables read or set by %forgemeta
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Tue Jun 01 2021 Zamir SUN <sztsian@gmail.com> - 1.7-2
- Add python3-requests into Requires

* Tue Jan 05 2021 Čestmír Kalina <ckalina@redhat.com> - 1.7-1
- Initial Fedora commit.
