#!/usr/bin/env bash

function cleanup()
{
	rlPhaseStartCleanup
	rm -rf "${TEMPFILES[@]}"
	rlPhaseEnd
}
trap cleanup 0 1 9 15

# --- Globals -----------------------------------------------------------------

SCRIPT_ROOT="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

# A list of temporary files; used by cleanup to delete on signals 0 1 9 15.
TEMPFILES=()

# A list of dependencies to include.
INCLUDES=()

# A list of files containing test definitions.
# These are auto-discovered using test-*.sh pattern.
TESTS_FILES=()

# A list of tests to run.
# These are automatically added by test files.
TESTS=()

# The following can be overriden to force a particular setting.

# RPM_BIN_DIR is not defined
# RPM_DATA_DIR is not defined
# KSC_BIN is not defined
# MOD_PATH is not defined
# MANUAL is not defined

# --- Initialization ----------------------------------------------------------

echo
echo " :: ksc Gating for RHEL"
echo

# Requires: restraint-rhts
. /usr/bin/rhts-environment.sh || exit 1
# Requires: beakerlib
. /usr/share/beakerlib/beakerlib.sh || exit 1

INCLUDES+=("$SCRIPT_ROOT/common-tests.sh")
INCLUDES+=("$SCRIPT_ROOT/common-symbols.sh")

TESTS_FILES+=("$SCRIPT_ROOT/test-"*".sh")

# --- bkr journal -------------------------------------------------------------

rlJournalStart

rlPhaseStartSetup

# --- Load dependencies -------------------------------------------------------

for path in ${INCLUDES[@]} ${TESTS_FILES[@]}
do
	if ! test -r $path
	then
		rlFail "Path \`$path' does not exist or is not readable"
		exit 1
	fi
        source $path && {
		rlPass "File \`$(basename "$path")' loaded."
	} || {
		rlFail "Unable to load \`$path'."
		exit 1
	}
done

# --- Temporary files ---------------------------------------------------------

echo ":: Initialization: Temporary files."

__stdout_log=$(mktemp -p /tmp ksc-test-stdout.XXXXX)
TEMPFILES+=("$__stdout_log")

__stderr_log=$(mktemp -p /tmp ksc-test-stderr.XXXXX)
TEMPFILES+=("$__stderr_log")

# --- Evaluate RPM-specific macros --------------------------------------------
# This is required not to hardcode ksc install location should %{_bindir} and
# %{_datadir} be changed.

echo ":: Initialization: Evaluating RPM macros."

if test -z "$RPM_BIN_DIR"
then
        RPM_BIN_DIR="$(rpm --eval '%{_bindir}')"
        echo "  * RPM %{_bindir} determined as: $RPM_BIN_DIR"
fi

if test -z "$RPM_DATA_DIR"
then
        RPM_DATA_DIR="$(rpm --eval '%{_datadir}')"
        echo "  * RPM %{_datadir} determined as: $RPM_DATA_DIR"
fi

# --- Determine ksc location --------------------------------------------------

if test -z "$KSC_BIN"
then
        KSC_BIN="$RPM_BIN_DIR/ksc"
        echo ":: ksc path determined as: $KSC_BIN"
fi

if test -z "$MOD_PATH"
then
        if [[ -L /lib && -d /lib ]]; then
                MOD_PATH=/usr/lib/modules/$(uname -r)/
        else
                MOD_PATH=/lib/modules/$(uname -r)/
        fi
        echo ":: Module path determined as: $MOD_PATH"
fi

rlPass "Initialization passed."

rlPhaseEnd

# --- Run tests ---------------------------------------------------------------

overall_status=0
for test in ${TESTS[@]}
do
        run_test $test "$__stdout_log" "$__stderr_log"
        if test $? -gt 0
        then
                overall_status=1
        fi
done

rlPhaseStartTest

if test $overall_status -gt 0
then
        rlFail "Some tests failed."
        exit 1
else
        rlPass "All tests passed."
fi
echo

rlPhaseEnd

rlJournalPrintText

rlJournalEnd

exit 0

# -- Future tests: pylint/flake8 on the python source itself

# First, we need to detect whether or not $KSC_BIN is a shell launcher or
# the python script itself. As of this moment, $KSC_BIN is a shell launcher,
# however, currently there is no real reason for that and might be changed
# in the future. To prepare for this alternative, and to be backwards compat
# if the change occurs, detect whether $KSC_BIN is the python executable script
# or a shell launcher and obtain path to the python script in the latter case.

KSC_PY=""
case $(file "$KSC_BIN" | awk -F'[,:] ' '{print $2;}') in
"POSIX shell script")
	# We're dealing with a launcher, get the python script itself
	KSC_PY="$(grep -o "[^ ]*ksc.py" "$KSC_BIN")"
	;;
"Python script")
	# We're dealing with a python script
	KSC_PY="$KSC_BIN"
	;;
esac

# TODO: Call flake8/pylint here

