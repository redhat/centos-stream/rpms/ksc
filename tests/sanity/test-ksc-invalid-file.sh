#!/usr/bin/env sh
#
# ksc gating
#
# verify that ksc fails when presented w/ non-existent path
#

TESTS+=(test_ksc_invalid_file)

DESCRIPTION_test_ksc_invalid_file=(
        "Verify that ksc fails when presented w/ non-existent path."
)

function test_ksc_invalid_file()
{
        local stdout_log="$1"
        local stderr_log="$2"
        shift 2

        local non_exist_file="/tmp/$RANDOM"
        while test -e $non_exist_file
        do
                non_exist_file=$non_exist_file$RANDOM
        done

        "$KSC_BIN" -k "$non_exist_file" >> $stdout_log 2>> $stderr_log

        if test $? -eq 0 -a ! -e "$non_exist_file"
        then
                echo
                echo "ERROR: $KSC_BIN returned w/ zero return code when" \
                     "presented w/ non-existent file."

                echo "STDOUT {"
                cat $stdout_log
                echo "}"

                echo "STDERR {"
                cat $stderr_log
                echo "}"

                return 1
        fi

	return 0
}