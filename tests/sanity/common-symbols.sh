#!/usr/bin/env bash

# Get all symbols from ksc result's nonstablelisted sections
function ksc_report_symbols_nonstablelisted() {
        sed -n 's/^.*(\([^)]*\))$/\1/p' ~/ksc-result.txt \
        | sort \
        | uniq
}

# Get all symbols from ksc result's stablelisted sections
function ksc_report_symbols_stablelisted() {
        sed -n '/^\[WHITELISTUSAGE\]$/,/^\[NONWHITELISTUSAGE\]$/p' \
               ~/ksc-result.txt \
        | grep -v '^\[' \
        | sort \
        | uniq
}

# Get all symbols from ksc result
function ksc_report_symbols_all() {
        {
                ksc_report_symbols_stablelisted
                ksc_report_symbols_nonstablelisted
        } |  sort | uniq
}

# Get undefined symbols in all argument-provided ko files
function ko_get_undefined() {
        echo -e ${@/%/\\n} \
        | xargs -I KO nm -u KO \
        | awk '{print $(NF);}' \
        | sort \
        | uniq
}

# Get defined symbols in all argument-provided ko files
function ko_get_defined() {
        echo -e ${@/%/\\n} \
        | xargs -I KO nm --defined-only KO \
        | awk '{print $(NF);}' \
        | sort \
        | uniq
}

# Get all undefined symbols for the group; i.e., symbols defined in one
# ko and undefined in another will be ignored; this captures ksc behaviour
# when used w/ multiple -k options.
function ko_get_group_undefined() {
        comm -23 <(ko_get_undefined "$@") <(ko_get_defined "$@")
}

# Get all symbols present in kABI stablelist
function kabi_stablelists_symbols() {
        grep -h '^[[:space:]]' /lib/modules/kabi-current/* \
        | tr -d '\t' \
        | sort \
        | uniq
}

# Get all symbols present in Module.symvers
function get_module_symvers_symbols() {
        awk '{print $2;}' /usr/src/kernels/$(uname -r)/Module.symvers \
        | sort \
        | uniq
}
