#!/usr/bin/env sh
#
# ksc gating
#
# verify that ksc manpage exists and is nonempty
#


TESTS+=(test_ksc_manpage)

DESCRIPTION_test_ksc_manpage=(
        "Verify that ksc manpage exists and is nonempty."
)

function test_ksc_manpage()
{
        local stdout_log="$1"
        local stderr_log="$2"
        shift 2

	LC_ALL=en_US.UTF-8 man -P cat ksc > $stdout_log 2> $stderr_log

	if test $? -gt 0
	then
                echo
		echo "$? ERROR: man ksc failed with non-zero" \
		     "return code. Expected zero."

		echo "STDOUT {"
		cat $stdout_log
		echo "}"

		echo "STDERR {"
		cat $stderr_log
		echo "}"

		return 1
	fi

        if test ! -s "$stdout_log"
	then
                echo
		echo "ERROR: man ksc produced no data on stdout." >&2
		return 1
	fi

	if test -s "$stderr_log"
	then
                echo
		echo "ERROR: man ksc produced data on stderr." >&2
		return 1
	fi

	return 0
}