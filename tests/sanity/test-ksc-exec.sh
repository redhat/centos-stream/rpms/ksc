#!/usr/bin/env sh
#
# ksc gating
#
# verify that ksc is executable
#


TESTS+=(test_ksc_executable)

DESCRIPTION_test_ksc_executable=(
        "Verify that ksc is executable."
)

function test_ksc_executable()
{
        local stdout_log="$1"
        local stderr_log="$2"
        shift 2

	if ! test -x "$KSC_BIN"
	then
                echo
		echo "ERROR: $KSC_BIN either does not exist or is not" \
		     "executable!" >&2
		ls -l "$KSC_BIN" >&2
		return 1
	fi
	return 0
}