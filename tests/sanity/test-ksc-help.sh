#!/usr/bin/env sh
#
# ksc gating
#
# verify that ksc -h (--help) provides usage and terminated w/ non-zero error
#


TESTS+=(test_ksc_help)

DESCRIPTION_test_ksc_help=(
        "Verify that ksc -h (--help) provides usage and terminated w/"
        "non-zero error."
)

function test_ksc_help()
{
        local stdout_log="$1"
        local stderr_log="$2"
        shift 2

	for arg in -h --help
	do
		echo > $stdout_log
		echo > $stderr_log

		echo "# Calling $KSC_BIN $arg" >> $stdout_log
		"$KSC_BIN" $arg >> $stdout_log 2>> $stderr_log

		if test $? -gt 0
		then
                        echo
			echo "ERROR: $KSC_BIN $arg failed with non-zero" \
			     "return code. Expected zero."

			echo "STDOUT {"
			cat $stdout_log
			echo "}"

			echo "STDERR {"
			cat $stderr_log
			echo "}"

			return 1
		fi

		if test ! -s "$stdout_log" -a ! -s "$stderr_log"
		then
                        echo
			echo "ERROR: $KSC_BIN $arg produced no data." >&2
			return 1
		fi

	done
	return 0
}