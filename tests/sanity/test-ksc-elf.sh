#!/usr/bin/env bash
#
# ksc gating
#
# verify that ksc launcher is marked executable
#

SYMBOL_CHECKS=()

SYMBOL_CHECKS+=(symbol_check_1)
DESCRIPTION_symbol_check_1=(
        "Verify that all symbols in stablelisted and nonstablelisted sections"
        "of the just produced ksc-report.txt are marked as undefined by nm,"
        "discounting symbols defined within the set of modules used."
)
function symbol_check_1() {
        test -z "$(comm -13 <(ksc_report_symbols_all) \
                            <(ko_get_group_undefined))"
        return $?
}

SYMBOL_CHECKS+=(symbol_check_2)
DESCRIPTION_symbol_check_2=(
        "Verify that all symbols in stablelisted and nonstablelisted sections"
        "of the just produced ksc-report.txt come from Module.symvers only."
)
function symbol_check_2() {
        test -z "$(comm -23 <(ksc_report_symbols_all) \
                            <(get_module_symvers_symbols))"
        return $?
}

# 
SYMBOL_CHECKS+=(symbol_check_3)
DESCRIPTION_symbol_check_3=(
        "Verify that all stablelisted symbols are present on kabi stablelist."
)
function symbol_check_3() {
        test -z "$(comm -13 <(kabi_stablelists_symbols) \
                            <(ksc_report_symbols_stablelisted))"
        return $?
}

function find_ko()
{
        if test -z $1
        then
                # TODO ERRROR
                return 1
        fi

        local count=$1
        local matches=()
        
        if test $count -gt 0
        then
                matches+=($(
                        find "$MOD_PATH" -type f -iname "*.ko*" \
                        | head -n $count \
                        | xargs -I MATCH bash -c '
                                case $(echo MATCH | grep -P "\.ko($|\.[^.]+$)") in
                                .ko.xz)  unxz     MATCH;;
                                .ko.bz2) bzip2 -d MATCH;;
                                .ko.gz)  gunzip   MATCH;;
                                *)       echo     MATCH; exit 0;;
                                esac;
                                echo MATCH | sed "s/\.[^.]*$//g";'
                ))
        fi

        # make sure some matches were found
        if test ${#matches[@]} -eq 0
        then
                echo "No modules found." >&2
                return 1
        fi

        # and that they are in fact readable
        for match in ${matches[@]}
        do
                if test -z $match -o ! -r $match
                then
                        # TODO ERRROR
                        return 1
                fi
        done
 
        echo ${matches[@]}

        return 0
}

function common_ksc_elf_ko()
{
        local test_name="$1"
        local stdout_log="$2"
        local stderr_log="$3"
        local count="$4"
        shift 4

        local ko=($(find_ko $count))

        eval DESCRIPTION_$test_name+=\(\"\"\)
        eval DESCRIPTION_$test_name+=\(\"Test used the following modules:\"\)
        [ ${#ko[@]} -gt 0 ] || { echo "ERROR: No module found in MOD_PATH=$MOD_PATH."; exit 1; }
        for mod in ${ko[@]}
        do
                mod_rel="$(realpath --relative-to="$MOD_PATH" $ko)"
                eval DESCRIPTION_$test_name+=\(\"\${mod_rel/\#/ - }\"\)
        done

        rm -f ~/ksc-result.txt
        "$KSC_BIN" ${ko[@]/#/-k } >> $stdout_log 2>> $stderr_log

        if test $? -gt 0
        then
                echo
                echo "ERROR: $KSC_BIN returned w/ non-zero return code when" \
                     "presented w/ valid kernel module."

                echo "STDOUT {"
                cat $stdout_log
                echo "}"

                echo "STDERR {"
                cat $stderr_log
                echo "}"

                return 1
        fi

        if test ! -s ~/ksc-result.txt
        then
                echo
                echo "ERROR: ~/ksc-result.txt was not created or is empty."

                echo "STDOUT {"
                cat $stdout_log
                echo "}"

                echo "STDERR {"
                cat $stderr_log
                echo "}"

                return 1
        fi

	return 0
}

TESTS+=(test_ksc_elf_ko_single ${SYMBOL_CHECKS[@]})

DESCRIPTION_test_ksc_elf_ko_single=(
        "Verify that ksc processes valid ELF kernel module. This amounts to"
        "checking that ~/ksc-result.txt is created and that ksc terminates"
        "w/ non-zero error code."
)

function test_ksc_elf_ko_single()
{
        common_ksc_elf_ko test_ksc_elf_ko_single $@ 1
        return $?
}

TESTS+=(test_ksc_elf_ko_mul)

DESCRIPTION_test_ksc_elf_ko_mul=(
        "Verify that ksc processes multiple valid ELF kernel modules. This"
        "amounts to checking that ~/ksc-result.txt is created and that ksc"
        "terminates w/ non-zero error code."
)

function test_ksc_elf_ko_mul()
{
        common_ksc_elf_ko test_ksc_elf_ko_mul $@ 2
        return $?
}